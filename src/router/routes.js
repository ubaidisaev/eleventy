export const routes = [
  {
    name: "index",
    path: "/",
    component: () => import("@/views/PageMeetups.vue"),
  },
  {
    name: "about",
    path: "/about",
    component: () => import("@/views/PageAbout.vue"),
  },
  {
    name: "meetups",
    path: "/meetups",
    redirect: { name: "index" },
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/LoginPage.vue"),
  },
  {
    path: "/meetups/:meetupId(\\d+)",
    name: "meetup",
    props: true,
    component: () => import("@/views/PageMeetup.vue"),
    redirect: (to) => {
      console.log("redirect");
      return { name: "meetup.description", params: to.params };
    },
    meta: {
      showReturnToMeetups: true,
    },
    children: [
      {
        props: true,
        path: "",
        alias: "description",
        name: "meetup.description",
        component: () => import("@/views/PageMeetupDescription.vue"),
      },
      {
        path: "agenda",
        name: "meetup.agenda",
        props: true,
        component: () => import("@/views/PageMeetupAgenda.vue"),
      },
    ],
  },
  {
    path: "/user/:id",
    name: "UserView",
    component: () => import("@/UserView.vue"),
  },
  // {
  //   name: "meetups",
  //   path: "/meetups",
  //   redirect: { name: "about" },
  // },
  // {
];
