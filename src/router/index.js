import { createRouter, createWebHistory } from "vue-router";
import { routes } from "./routes";

export const router = createRouter({
  routes,
  history: createWebHistory(import.meta.env.BASE_URL),
  linkActiveClass: 'active',
});
