import { defineStore } from 'pinia'


export const useTodosStore = defineStore({
    id: "todos",
    state: () => ({
        todos: [],
    }),
    // getters: {
    //     todos: (state) => {
    //         return state.todos;
    //     }
    // },
    actions: {
        addItem(item) {
            this.todos.push(item);
        },
    }
});