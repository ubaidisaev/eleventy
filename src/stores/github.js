import { defineStore } from "pinia";
import axios from "axios";

const baseUrl = "https://api.github.com/search/users?q=";

export const useGithubStore = defineStore({
  id: "github",
  state: () => ({
    users: [],
    isLoading: false,
  }),
  // getters: {
  //     todos: (state) => {
  //         return state.todos;
  //     }
  // },
  actions: {
    async findUsers(searchField) {
      try {
        this.isLoading = true;
        const response = await axios.get(baseUrl + searchField);
        this.users = response.data.items;
        this.isLoading = false;
      } catch (error) {
        console.error(error);
      }
    },
  },
});
