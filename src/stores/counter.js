import { defineStore } from "pinia";

export const useCounterStore = defineStore({
  id: "counter",
  state: () => ({
    counter: 0,
    todos: [],
  }),
  getters: {
    doubleCount: (state) => state.counter * 2,
  },
  actions: {
    addItem(item) {
      // this.todos = ['ivan smirnov'];
      // console.log('this.todos', this.todos);
      this.todos.push(item)
    },
    increment() {
      this.counter++;
    },
    decrement() {
      this.counter--;
    }
  },
});
